<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
        <?php
        require 'Form.php';
        require 'Validator.php';
        $Formulaire = new Form();
        $Formulaire->OpenForm('#','POST', 'class="Coucou"');
        $Formulaire->InputText('Test', 'id', 'value="Coucou"', 'LeLabel', 'class="coucou"');
        echo '<br><br>';
        $Formulaire->OptionAttr('class="1"', '', 'class="3"', 'class="4"','');
        $Formulaire->Select('LeSelect', '', 'Valeur 1', 'Valeur 2', 'Valeur 3', 'Valeur 4', 'Valeur 5');
        echo '<br><br>';
        $Formulaire->TextArea('textarea', 'area', '10', '50', 'class="coucou"', 'Label Area', 'id="labelarea"');
        echo '<br><br>';
        $Formulaire->RadioAttr('class="radio class" id="Coucou"', '');
        $Formulaire->Radio('radioname', '', 'Radio 1', 'Radio 2');
        echo '<br><br>';
        $Formulaire->CheckAttr('class="Check class" id="Coucou"', '');
        $Formulaire->Checkbox('checkname', '', 'Check 1', 'Check 2');
        echo '<br><br>';
        $Formulaire->Submit('Envoyer', 'class="coucou"');
        $Formulaire->CloseForm();
        $valid = new Validator();
        $valid->Regex("Tel", $_POST["Test"]);
        $valid->RegexPerso("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", "Email", $_POST["Test"])
        ?>
    </body>
</html>
