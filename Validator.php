<?php
class Validator{

    private $Regex;

    /**
     * @return mixed
     */
    public function Regex($leRegex, $Verification)
    {
        switch ($leRegex) {
            case "Email":
                $this->Regex = "#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#";
                break;
            case "Tel":
                $this->Regex = "/^(0|\+330|0033|\+33[-. ]?0|\+33|\(\+33\)[-. ]?0)[1-9](([-][0-9]{2}){4}|([0-9]{2}){4}|([.][0-9]{2}){4}|([ ][0-9]{2}){4})$/";
                break;
            /*case "":
                $this->Regex = "";
                break;*/
            default:
                echo 'Veuillez entrer un nom de Regex valide.';
        }

        if (preg_match($this->Regex, $Verification)) {
            echo "ok";
        } else {
            echo "Veuillez entrer un ".$leRegex." valide.";
        }
    }

    public function RegexPerso($leRegex, $NomRegex, $Verification)
    {
        $this->Regex = $leRegex;

        if (preg_match($this->Regex, $Verification)) {
            echo "ok";
        } else {
            echo "Veuillez entrer un ".$NomRegex." valide.";
        }
    }
}
?>