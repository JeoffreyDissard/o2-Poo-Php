<?php
class Form{

    private $action;
    private $method;
    private $autre;
    private $name;
    private $id;
    private $label;
    private $autre_label;
    private $option;
    private $attr;
    private $value;
    private $cols;
    private $rows;
    private $radioattr;
    private $radio;
    private $checkattr;
    private $check;

    public function __construct()
    {
    }

    /**
     * Méthode OpenForm permet d'ouvrir une balise form et de lui mettre l'action et la method,
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre.
     * Exemple pour $autre : 'class="Coucou" id="recoucou"'
     */
    public function OpenForm($action, $method, $autre=null)
    {
        $this->action = $action;
        $this->method = $method;
        $this->autre = $autre;

        echo '<form action="'.$this->action.'" method="'.$this->method.'" '.$this->autre.'>';
    }
    /**
     * Méthode OpenForm permet de fermer la balise form
     */
    public function CloseForm()
    {
        echo '</form>';
    }



    /**
     * Méthode InputText permet d'ouvrir une balise input de type text et de lui mettre un name et un id,
     * il est possible de rajouter une balise label en completant l'argument $label
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour l'input
     * et dans $autre_label pour le label
     * Exemple pour $autre et $autre_label: 'class="Coucou" id="recoucou"'
     */
    public function InputText($name, $id, $autre=null, $label=null, $autre_label=null)
    {
        $this->name = $name;
        $this->id = $id;
        $this->autre = $autre;
        $this->label = $label;
        $this->autre_label = $autre_label;

        if (isset($label)) echo '<label for="'.$this->id.'" '.$this->autre_label.'>'.$this->label.'</label><br>';

        echo '<input type="text" id="'.$this->id.'" name="'.$this->name.'" '.$this->autre.'>';
    }



    /**
     * Méthode Select permet d'ouvrir une balise select et de lui mettre un name,
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour le select
     * Exemple pour $autre: 'class="Coucou" id="recoucou"'
     *Il est possible d'ajouter des <option>, il suffit de les ajouter en arguments à la suite.
     *
     * Il est possible d'ajouter des attributs dans les <option> en utilisant la méthode OptionAttr,
     * Il faut appeller la méthode OptionAttr avant la méthode Select
     * cependant il faut ajouter le même nombres d'attributs et dans le même ordre que les <option>
     */
    public function OptionAttr(...$attr)
    {
        $this->attr = $attr;
    }

    public function Select($name, $autre=null, ...$option)
    {
        $this->option = $option;
        $this->name = $name;
        $this->autre = $autre;

        echo '<select name="'.$this->name.'" '.$this->autre.'>';
        $i = 0;
        foreach ($this->option as $val) {
            echo '<option value="'.$val.'" '.$this->attr[$i].'>'.$val.'</option>';
            $i++;
        }
        echo '</select>';
    }



    /**
     * Méthode Submit permet d'ouvrir une balise button type submit et de lui mettre une value,
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour le select
     * Exemple pour $autre: 'class="Coucou" id="recoucou"'
     */
    public function Submit($value, $autre=null)
    {
        $this->autre = $autre;
        $this->value = $value;
        echo '<button type="submit" value="'.$this->value.'" '.$this->autre.'>'.$this->value.'</button>';
    }



    /**
     * Méthode TextArea permet d'ouvrir une balise textarea et de lui mettre un name, un id, un cols et un rows.
     * il est possible de rajouter une balise label en completant l'argument $label
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour l'input
     * et dans $autre_label pour le label
     * Exemple pour $autre et $autre_label: 'class="Coucou" id="recoucou"'
     */
    public function TextArea($name, $id, $rows, $cols, $autre=null, $label=null, $autre_label=null)
    {
        $this->name = $name;
        $this->id = $id;
        $this->rows = $rows;
        $this->cols = $cols;
        $this->autre = $autre;
        $this->label = $label;
        $this->autre_label = $autre_label;

        if (isset($label)) echo '<label for="'.$this->id.'" '.$this->autre_label.'>'.$this->label.'</label><br>';

        echo '<textarea id="'.$this->id.'" name="'.$this->name.'" rows="'.$this->rows.'" cols="'.$this->cols.'" '.$this->autre.'></textarea>';
    }



    /**
     * Méthode Radio permet d'ouvrir une balise input type radio et de lui mettre un name,
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour l'input
     * Exemple pour $autre: 'class="Coucou" id="recoucou"'
     *Il est possible d'ajouter des <input>, il suffit de les ajouter en arguments à la suite.
     *
     * Il est possible d'ajouter des attributs dans les <input> en utilisant la méthode RadioAttr,
     * Il faut appeller la méthode RadioAttr avant la méthode Radio
     * cependant il faut ajouter le même nombres d'attributs et dans le même ordre que les <input>
     */
    public function RadioAttr(...$radioattr)
    {
        $this->radioattr = $radioattr;
    }
    public function Radio($name, $autre=null, ...$radio)
    {
        $this->radio = $radio;
        $this->name = $name;
        $this->autre = $autre;

        $i = 0;
        foreach ($this->radio as $val) {
            echo '<input type="radio" name="'.$this->name.'" value="'.$val.'" '.$this->radioattr[$i].'>'.$val.'<br>';
            $i++;
        }
        echo '</select>';
    }



    /**
     * Méthode Check permet d'ouvrir une balise input type checkbox et de lui mettre un name,
     * de plus il est possible de rajouter d'autre attributs dans l'argument $autre pour l'input
     * Exemple pour $autre: 'class="Coucou" id="recoucou"'
     *Il est possible d'ajouter des <input>, il suffit de les ajouter en arguments à la suite.
     *
     * Il est possible d'ajouter des attributs dans les <input> en utilisant la méthode CheckAttr,
     * Il faut appeller la méthode CheckAttr avant la méthode Check
     * cependant il faut ajouter le même nombres d'attributs et dans le même ordre que les <input>
     */
    public function CheckAttr(...$checkattr)
    {
        $this->checkattr = $checkattr;
    }
    public function Checkbox($name, $autre=null, ...$check)
    {
        $this->check = $check;
        $this->name = $name;
        $this->autre = $autre;

        $i = 0;
        foreach ($this->check as $val) {
            echo '<input type="checkbox" name="'.$this->name.'" value="'.$val.'" '.$this->checkattr[$i].'>'.$val.'<br>';
            $i++;
        }
        echo '</select>';
    }

}
?>